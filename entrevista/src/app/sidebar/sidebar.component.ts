import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  user$: Object;
  servicios$: Object;
  consumo$:Object;

  constructor() { }

  ngOnInit() {
    this.user$ = {	"usuario": "Carlos Lopez",	"ultimaConexion": "9 de mayo de 2018",	"saldoTotal": "388,47"};
    this.servicios$=[{"servicio": "energia","pagado": "si"}, {"servicio": "telefonia celular","pagado": "si"}, {"servicio": "telefonia fija","pagado": "si"}, {"servicio": "prepago","pagado": "si"}, {"servicio": "otro servicio","pagado": "si"}];
    this.consumo$={"id":"1234","descripcion":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","atraso":"0","consumo":"12345","saldo":"388,47","mora":"46,89"};
  }

}
